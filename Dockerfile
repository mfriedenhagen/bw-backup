FROM multiarch/alpine:armhf-latest-stable
LABEL org.label-schema.url="https://gitlab.com/mfriedenhagen/bw-backup"\
      org.label-schema.vcs-url="https://gitlab.com/mfriedenhagen/bw-backup"\
      org.label-schema.schema-version="1.0"\
      org.label-schema.docker.debug="docker run --rm -it --entrypoint=/bin/sh registry.gitlab.com/mfriedenhagen/bw-backup/master"\
      org.label-schema.name="registry.gitlab.com/mfriedenhagen/bw-backup"\
      org.label-schema.description='"Backup service" for sqlite3 backed vaultwarden on a raspberrypi.'\
      maintainer="https://gitlab.com/mfriedenhagen"

RUN apk --no-cache add curl sqlite
COPY bw-backup.sh /usr/bin/bw-backup
ENTRYPOINT ["/usr/bin/bw-backup"]
CMD ["%a.%H"]
