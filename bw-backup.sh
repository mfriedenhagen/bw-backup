#!/bin/sh -e
export LC_ALL=C

# Check prerequisites
[ -r /db.sqlite3 ] || { >&2 echo "database /db.sqlite3 not found" ; exit 1 ; }
[ -r /bw-backup -a -d /bw-backup ] || { >&2 echo "backup-dir /bw-backup not found" ; exit 1 ; }
[ "${BW_WEBDAV_URL}" ] || { >&2 echo "BW_WEBDAV_URL needs to be set" ; exit 1 ; }
[ -r /root/.netrc ] || { >&2 echo "/root/.netrc needs to be readable" ; exit 1 ; }

# Check password is in .netrc
webdav_host="`echo $BW_WEBDAV_URL | cut -d/ -f3`"
grep -q "${webdav_host}" /root/.netrc || { >&2 echo "${webdav_host} not found in .netrc" ; exit 1 ; }
date_format=${1}

# Calc target filepath and
extension="`date +${date_format}`"
target=db.sqlite3.${extension}

# Finally work :-)
/usr/bin/sqlite3 /db.sqlite3 ".backup /bw-backup/${target}"
echo >&2 "/bw-backup/${target} created"
curl --silent --show-error --fail --netrc -X PUT -T /bw-backup/${target} ${BW_WEBDAV_URL}/${target}
echo >&2 "${BW_WEBDAV_URL}/${target} uploaded"
