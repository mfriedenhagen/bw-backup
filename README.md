# bw-backup

* "Backup service" for e.g. a sqlite3 backed [vaultwarden](https://github.com/dani-garcia/vaultwarden) on a raspberrypi.
* Creates firstly a local backup to directory `/bw-backup` and afterwards uploads the created file via `PUT`
  to the specified location `${BW_WEBDAV_URL}/`

* Note that the target directory `/bw-backup` MUST be created beforehand and be writable.
* The sqlite DB MUST be mounted at `/db.sqlite3` and SHOULD be mounted read-only.
* [`netrc`](https://linux.die.net/man/5/netrc) MUST be mounted to the `HOME` of the executing user (e.g. `/root/.netrc`) and SHOULD be mounted read-only.
```
machine webdav.example.com login aLogin password aPassword 
```
* The name of the backup file is `db.sqlite3.${extension}`
* `extension` is calculated from a given [`date` pattern](https://linux.die.net/man/1/date)) and defaults to `%a.%H`, i.e. if you run
  this command twice per day via CRON at 03:00 and 15:00, the names would be `db.sqlite3.Mon.03`,
  `db.sqlite3.Mon.15`, `db.sqlite3.Tue.03` etc. That is you get 14 backups per week before the oldest
  one is overridden.   
* You may specify alternate formats as argument/`CMD`   

```sh
# Create a backup with the ISO week number as suffix, the SQLite DB is named `foo` and use the executing
# users .netrc
docker run --rm \
  -e BW_WEBDAV_URL=https://webdav.example.com/bw-backup \
  -v $HOME/.netrc:/root/.netrc:ro \
  -v $PWD/foo:/db.sqlite3:ro \
  -vbw-backup:/bw-backup \
  registry.gitlab.com/mfriedenhagen/bw-backup %V
```
